<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'samsung');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '7TWaskME');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-JBp(m!7CGYR*]Y2e,1=e>[g:@b3w;+aYY-2:vW*0v&-E.!N*?hBeZEFBX#-[H ^');
define('SECURE_AUTH_KEY',  '~YiDom@hc5gZj%/bAZl>K!dvl:,Fjp*Q<kl.0XtjhXCd=;A;GK8|2)Di vP?y7Zj');
define('LOGGED_IN_KEY',    'U+ha[AT8I+G$U|e%$=37i#+-,*Kr<;}({~>i~p8H%u/?K{<z]8 Boc2$u25w-+<o');
define('NONCE_KEY',        '3{~|t9CyneHHV^>4t2&s|rRv|=Bb!Z{Qm}>9n^W:$Vro0<_lA+O14m/y6@OYee X');
define('AUTH_SALT',        '$rH~y :AgA+e$64edT5+n(l+L`D|++Zp%i8G%@+Qj>m1N7u5Y2O.5+B*hj:>*>85');
define('SECURE_AUTH_SALT', 'LnB27+p,(K)5kObi!`Td0Y=*S{:Jc_( DpaIXfuWr#`VF-:,o[s[8sZwZ,3g:R}|');
define('LOGGED_IN_SALT',   'z H)7S~ 9gsxXXv1|F6K-cT<A$j;sk7,c|F<Kn3.+~]IZ!znNJgv%?}SnZaah<VI');
define('NONCE_SALT',       ')S+3sxzD{}iV^r-Qz+a76uP^0Gq $Z=CNg<R4~ka]D~h*#p=S]/?5}G5R|~gFCWD');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
