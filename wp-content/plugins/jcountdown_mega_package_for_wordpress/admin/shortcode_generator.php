﻿<?php
if (!class_exists('jCountdown')){
	return;
}
?>
<div class="wrap">
    <div id="shortcodeGeneratorContent">
        <div id="icon-tools" class="icon32"><br></div>
        <h2>Shortcode Generator</h2>
        <div id="tip">
            <div class="arrow"></div>
            <div class="content"></div>
        </div>
        <div class="page">
            <div class="popup createShortcode">
                <div class="window">
                    <div class="title">Create Shortcode</div>
                    <div class="tip">Press CTRL+C to Copy<br />Paste it in a Post, Page or Text Widget</div>
                    <textarea class="content" readonly="readonly"></textarea>
                    <div class="controls">
                        <div class="button fullLine close">Close</div>
                    </div>
                </div>
            </div>
            <div class="popup loadShortcode">
                <div class="window">
                    <div class="title">Load Shortcode</div>
                    <div class="tip">Paste Shortcode Here<br />Click Load Button to Load Setting</div>
                    <textarea class="content"></textarea>
                    <div class="controls">
                        <div class="button load">Load</div>
                        <div class="button close">Cancel</div>
                    </div>
                </div>
            </div>
            <div class="countdown"></div>
            <div class="backgroundPicker" tip="check how it looks in different background color">
                <div class="button black"></div>
                <div class="button white"></div>
                <div class="button blue"></div>
                <div class="button orange"></div>
                <div class="button red"></div>
            </div>
            <ul class="panel">
                <li>
                    <div class="group" tip="Year/Month/Date Hour:Minute:Second">
                        <div class="title">Time Text</div>
                        <div class="content"><input type="text" class="timeText"/></div>
                    </div>
                    <div class="group" tip="GMT number">
                        <div class="title">Time Zone</div>
                        <div class="content"><input type="text" class="timeZone"/></div>
                    </div>
                </li>
                <li>
                    <div class="group">
                        <div class="title">Style</div>
                        <div class="content">
                            <div class="label"><input type="radio" class="style" value="flip"/>&nbsp;Flip</div>
                            <div class="label"><input type="radio" class="style" value="slide"/>&nbsp;Slide</div>
                        </div>
                        <div class="title"></div>
                        <div class="content">
                            <div class="label"><input type="radio" class="style" value="metal"/>&nbsp;Metal</div>
                            <div class="label"><input type="radio" class="style" value="crystal"/>&nbsp;Crystal</div>
                        </div>
                    </div>
                    <div class="group">
                        <div class="title">Color</div>
                        <div class="content">
                            <div class="label"><input type="radio" class="color" value="black" />&nbsp;Black</div>
                            <div class="label"><input type="radio" class="color" value="white"/>&nbsp;White</div>
                        </div>
                    </div>
                </li>
                <li tip="if you don't want to resize it please set it to Auto">
                    <div class="group">
                        <div class="title">Width</div>
                        <div class="content">
                            <div class="label"><input type="radio" class="widthType" value="auto" />&nbsp;Auto</div>
                            <div class="label"><input type="radio" class="widthType" value="fixed"/>&nbsp;Fixed</div>
                        </div>
                    </div>
                    <div class="group">
                        <div class="title"></div>
                        <div class="content"><div class="value"></div><div class="slider width"></div></div>
                    </div>
                </li>
                <li>
                    <div class="group">
                        <div class="title">Text Group Space</div>
                        <div class="content"><div class="value"></div><div class="slider textGroupSpace"></div></div>
                    </div>
                    <div class="group">
                        <div class="title">Text Space</div>
                        <div class="content"><div class="value"></div><div class="slider textSpace"></div></div>
                    </div>
                </li>
                <li>
                    <div class="group" tip="this setting works in Firefox, Chrome, Safari, Opera and most mobile browser">
                        <div class="title">Reflection</div>
                        <div class="content"><input type="checkbox" class="reflection"/></div>
                    </div>
                    <div class="group" tip="0~100 (default 10), this setting works in Firefox, Chrome, Safari, Opera and most mobile browser">
                        <div class="title">Reflection Opacity</div>
                        <div class="content"><div class="value"></div><div class="slider reflectionOpacity"></div></div>
                    </div>
                    <div class="group" tip="0~10 (default 0), this setting works in Firefox, Chrome and most mobile browser">
                        <div class="title">Reflection Blur</div>
                        <div class="content"><div class="value"></div><div class="slider reflectionBlur"></div></div>
                    </div>
                </li>
                <li>
                    <div class="group">
                        <div class="title">Day Text number</div>
                        <div class="content"><div class="value"></div><div class="slider dayTextNumber"></div></div>
                    </div>
                </li>
                <li>
                    <div class="group">
                        <div class="title">Display Day</div>
                        <input type="checkbox" class="displayDay"/>
                    </div>
                    <div class="group">
                        <div class="title">Display Hour</div>
                        <input type="checkbox" class="displayHour"/>
                    </div>
                    <div class="group">
                        <div class="title">Display Minute</div>
                        <input type="checkbox" class="displayMinute"/>
                    </div>
                    <div class="group">
                        <div class="title">Display Second</div>
                        <input type="checkbox" class="displaySecond"/>
                    </div>
                </li>
                <li>
                    <div class="group">
                        <div class="title">Display Label</div>
                        <input type="checkbox" class="displayLabel"/>
                    </div>
                </li>
                <li tip="redirect URL when time's up, if you don't want to redirect the URL please don't input anything">
                    <div class="group">
                        <div class="title fullLine">Redirect URL (e.g. http://google.com/)</div>
                    </div>
                    <div class="group">
                        <input type="text" class="onFinishRedirectURL fullLine"/>
                    </div>
                </li>
                <li>
                    <div class="group">
             	       <div class="button fullLine createShortcode">Create Shortcode</div>
                    </div>
                    <div class="group">
                    	<div class="button fullLine loadShortcode">Load Shortcode</div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>