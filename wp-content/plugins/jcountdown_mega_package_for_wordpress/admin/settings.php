﻿<?php
if (!class_exists('jCountdown')){
	return;
}
?>
<div class="wrap">
	<div id="settingsContent">
        <div id="icon-options-general" class="icon32"><br></div><h2>Settings</h2>
        <?php if($save==1){?>
        <div class="updated"> 
            <p><strong>Settings saved.</strong></p>
        </div>
        <?php } ?>
        <form method="post">
            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row"><span class="title">Disable all countdowns</span></th>
                        <td><input type="checkbox" name="disable_all_countdowns" value="1" <?php echo $adminOptions['disableAllCountdowns']==1 ? 'checked="checked"' : '' ?>/></td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><span class="title">Do not redirect URL</span></th>
                        <td><input type="checkbox" name="do_not_redirect_url" value="1" <?php echo $adminOptions['doNotRedirectURL']==1 ? 'checked="checked"' : '' ?>/></td>
                    </tr>
                </tbody>
            </table>
            <p class="submit"><input name="submit" id="submit" class="button-primary" value="Save Changes" type="submit"></p>
        </form>
        <hr />
        <form method="post">
            <input type="hidden" name="reset_to_default" value="1"/>
            <p class="submit"><input name="submit" id="submit" class="button-secondary" value="Reset All Settings To Default" type="submit"></p>
        </form>
	</div>
</div>