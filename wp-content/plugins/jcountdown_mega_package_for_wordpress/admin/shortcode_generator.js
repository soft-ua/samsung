jQuery(function() {
	var now = new Date();
	var nextDayText = now.getFullYear()+"/"+(now.getMonth()+1)+"/"+(now.getDate()+1)+" "+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
	var nextYearText = (now.getFullYear()+1)+"/"+(now.getMonth()+1)+"/"+now.getDate()+" "+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
	var next10000YearText = (now.getFullYear()+10000)+"/"+(now.getMonth()+1)+"/"+now.getDate()+" "+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
	var timeZone = -now.getTimezoneOffset()/60;

	var settings = [{
		timeText:nextYearText,
		timeZone:timeZone,
		style:"flip",
		color:"black",
		width:0,
		textGroupSpace:15, 
		textSpace:0,
		reflection:true,
		reflectionOpacity:10,
		reflectionBlur:0,
		dayTextNumber:3,
		displayDay:true,
		displayHour:true,
		displayMinute:true,
		displaySecond:true,
		displayLabel:true,
		onFinish:function(){
		}
	}];
	
	//init pages
	var initPages = function(){
		var content = jQuery("#shortcodeGeneratorContent");
		var page = jQuery("#shortcodeGeneratorContent>.page");

		for(var i=0; i<settings.length-1; i++){
			var setting = settings[i];
			var newPage = page.clone();
			var countdown = newPage.children(".countdown");
			countdown.jCountdown(setting);
			
			content.append(newPage);
		}
	}
	initPages();
	
	//check panel
	var checkPanel = function(page){
		var panel = page.children(".panel");
		var countdown = page.children(".countdown");

		var timeText = panel.find(".timeText").val();		
		var timeZone = parseFloat(panel.find(".timeZone").val());
		var style = panel.find(".style[checked]").val();
		var color = panel.find(".color[checked]").val();
		var width = panel.find(".widthType[checked]").val()=="auto" ? 0 : panel.find(".width").slider("value");
		var textGroupSpace = panel.find(".textGroupSpace").slider("value");
		var textSpace = panel.find(".textSpace").slider("value");
		var reflection = panel.find(".reflection").attr("checked")=="checked";		
		var reflectionOpacity = panel.find(".reflectionOpacity").slider("value");
		var reflectionBlur = panel.find(".reflectionBlur").slider("value");
		var dayTextNumber = panel.find(".dayTextNumber").slider("value");
		var displayDay = panel.find(".displayDay").attr("checked")=="checked";
		var displayHour = panel.find(".displayHour").attr("checked")=="checked";
		var displayMinute = panel.find(".displayMinute").attr("checked")=="checked";
		var displaySecond = panel.find(".displaySecond").attr("checked")=="checked";
		var displayLabel = panel.find(".displayLabel").attr("checked")=="checked";
		var onFinishRedirectURL = panel.find(".onFinishRedirectURL").val();
		
		if(timeText.split("/").join(" ").split(":").join(" ").split(" ").length!=6){
			panel.find(".timeText").addClass("error");
			timeText = "";
		}else{
			panel.find(".timeText").removeClass("error");
		}
		if(isNaN(timeZone)){
			panel.find(".timeZone").addClass("error");
			timeZone = 0;
		}else{
			panel.find(".timeZone").removeClass("error");
		}
		if(onFinishRedirectURL!="" && !/^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(onFinishRedirectURL)){
			panel.find(".onFinishRedirectURL").addClass("error");
			onFinishRedirectURL = "";
		}else{
			panel.find(".onFinishRedirectURL").removeClass("error");
			onFinishRedirectURL = encodeURIComponent(panel.find(".onFinishRedirectURL").val());
		}
		
		reflection ? panel.find(".reflectionOpacity").parents(".group").show() : panel.find(".reflectionOpacity").parents(".group").hide();
		reflection ? panel.find(".reflectionBlur").parents(".group").show() : panel.find(".reflectionBlur").parents(".group").hide();
		
		var setting = {
			timeText:timeText,
			timeZone:timeZone,
			style:style,
			color:color,
			width:width,
			textGroupSpace:textGroupSpace,
			textSpace:textSpace,
			reflection:reflection,
			reflectionOpacity:reflectionOpacity,
			reflectionBlur:reflectionBlur,
			dayTextNumber:dayTextNumber,
			displayDay:displayDay,
			displayHour:displayHour,
			displayMinute:displayMinute,
			displaySecond:displaySecond,
			displayLabel:displayLabel,
			onFinishRedirectURL:onFinishRedirectURL
		};
		
		return setting;
	}

	//submit panel data
	var submitPanel = function(page){
		var panel = page.children(".panel");
		var countdown = page.children(".countdown");
		
		var setting = checkPanel(page);
		
		countdown.jCountdown(setting);
		
		panel.find(".width").slider("value", countdown.children().width());
		panel.find(".width").parents(".group").find(".value").html(countdown.children().width());
	}
	
	//load shortcode setting
	var loadShortcodeSetting = function(page){
		var popup = page.children(".popup.loadShortcode");
		var panel = page.children(".panel");
		var countdown = page.children(".countdown");
		
		var shortcodeInfo 						= {};
		shortcodeInfo["timetext"] 				= {name:"timeText", type:"String"};
		shortcodeInfo["timezone"] 				= {name:"timeZone", type:"Number"};
		shortcodeInfo["style"] 					= {name:"style", type:"String"};
		shortcodeInfo["color"] 					= {name:"color", type:"String"};
		shortcodeInfo["width"] 					= {name:"width", type:"Number"};
		shortcodeInfo["textgroupspace"] 		= {name:"textGroupSpace", type:"Number"};
		shortcodeInfo["textspace"] 				= {name:"textSpace", type:"Number"};
		shortcodeInfo["reflection"] 			= {name:"reflection", type:"Boolean"};
		shortcodeInfo["reflectionopacity"]		= {name:"reflectionOpacity", type:"Number"};
		shortcodeInfo["reflectionblur"] 		= {name:"reflectionBlur", type:"Number"};
		shortcodeInfo["daytextnumber"] 			= {name:"dayTextNumber", type:"Number"};
		shortcodeInfo["displayday"] 			= {name:"displayDay", type:"Boolean"};
		shortcodeInfo["displayhour"] 			= {name:"displayHour", type:"Boolean"};
		shortcodeInfo["displayminute"] 			= {name:"displayMinute", type:"Boolean"};
		shortcodeInfo["displaysecond"] 			= {name:"displaySecond", type:"Boolean"};
		shortcodeInfo["displaylabel"] 			= {name:"displayLabel", type:"Boolean"};
		shortcodeInfo["onfinishredirecturl"]	= {name:"onFinishRedirectURL", type:"URL"};
		
		var setting = {};
		var code = popup.find(".content").val();
		var attrReg = /[\S]+?\s*=\s*".*?"/ig;
		var codeAttrs = code.match(attrReg);
		
		if(codeAttrs==null){
			return;
		}
		
		for(var j=0;j<codeAttrs.length;j++){
			var attr = codeAttrs[j];
			var index = attr.indexOf("=");
	
			var attrName = attr.substr(0,index);
			var attrValue = attr.substr(index+2,attr.length-index-3);
			var attrInfo = shortcodeInfo[attrName];
			
			if(attrInfo!=undefined){
				switch(attrInfo.type){
					case "String":
						break;
					case "Number":
						attrValue = parseFloat(attrValue);
						break;
					case "Boolean":
						attrValue = attrValue=="true";
						break;
					case "URL":
						attrValue = decodeURIComponent(attrValue);
						break;
					default:
				}
				
				setting[attrInfo.name] = attrValue;
			}
		}
		
		countdown.jCountdown(setting);
		
		panel.find("input").removeAttr("disabled");
		panel.find(".timeText").val(setting.timeText);
		panel.find(".timeZone").val(setting.timeZone);
		panel.find(".style").removeAttr("checked");
		panel.find(".style[value="+setting.style+"]").attr("checked","checked");
		panel.find(".color").removeAttr("checked");
		panel.find(".color[value="+setting.color+"]").attr("checked","checked");
		panel.find(".widthType").removeAttr("checked");
		setting.width==0 ? panel.find(".widthType[value=auto]").attr("checked","checked") : panel.find(".widthType[value=fixed]").attr("checked","checked");
		setting.width==0 ? panel.find(".width").parents(".group").hide() : panel.find(".width").parents(".group").show();
		panel.find(".width").parents(".group").find(".value").html(countdown.children().width());
		panel.find(".width").slider({min:50, max:900, step:1, value:countdown.children().width()});
		panel.find(".textGroupSpace").slider({min:0, max:100, step:1, value:setting.textGroupSpace});
		panel.find(".textSpace").slider({min:0, max:50, step:1, value:setting.textSpace});
		panel.find(".reflection").attr("checked",setting.reflection);
		panel.find(".reflectionOpacity").slider({min:0, max:100, step:1, value:setting.reflectionOpacity});
		setting.reflection ? panel.find(".reflectionOpacity").parents(".group").show() : panel.find(".reflectionOpacity").parents(".group").hide();
		panel.find(".reflectionBlur").slider({min:0, max:10, step:1, value:setting.reflectionBlur});
		setting.reflection ? panel.find(".reflectionBlur").parents(".group").show() : panel.find(".reflectionBlur").parents(".group").hide();
		panel.find(".dayTextNumber").slider({min:2, max:8, step:1, value:setting.dayTextNumber});
		panel.find(".displayDay").attr("checked",setting.displayDay);
		panel.find(".displayHour").attr("checked",setting.displayHour);
		panel.find(".displayMinute").attr("checked",setting.displayMinute);
		panel.find(".displaySecond").attr("checked",setting.displaySecond);
		panel.find(".displayLabel").attr("checked",setting.displayLabel);
		panel.find(".onFinishRedirectURL").val(setting.onFinishRedirectURL);

		panel.find(".textGroupSpace").parent().find(".value").html(setting.textGroupSpace);
		panel.find(".textSpace").parent().find(".value").html(setting.textSpace);
		panel.find(".reflectionOpacity").parent().find(".value").html(setting.reflectionOpacity);
		panel.find(".reflectionBlur").parent().find(".value").html(setting.reflectionBlur);
		panel.find(".dayTextNumber").parent().find(".value").html(setting.dayTextNumber);
	}
	
	//close all window
	var closeAllWindows = function(page){
		page.find(".popup").stop(true,true).fadeOut();
	}
	
	//popup shortcode window
	var popupCreateShortcodeWindow = function(page){
		var panel = page.children(".panel");
		var popup = page.children(".popup.createShortcode");
		
		var setting = checkPanel(page);
		
		var args = [];
		for(var attr in setting){
			args.push(attr+"="+'"'+setting[attr].toString()+'"');
		}
		code = "[jCountdown "+args.join(" ")+"]"+setting.timeText+"[/jCountdown]";
		code = code.toLowerCase();
		
		closeAllWindows(page);
		popup.find(".content").val(code);
		popup.stop(true,true).fadeIn();
		popup.find(".content").select();
	}
	
	//popup load shortcode window
	var popupLoadShortcodeWindow = function(page){
		var panel = page.children(".panel");
		var popup = page.children(".popup.loadShortcode");
		popup.find(".content").val("");
		
		closeAllWindows(page);
		
		popup.stop(true,true).fadeIn();
		popup.find(".content").focus();
	}

	//init panels
	var initPanels = function(){
		jQuery(".page").each(function(index, element){
			var page = jQuery(element);
			var countdown = page.children(".countdown");
			var panel = page.children(".panel");
			var popup = page.children(".popup");

			var setting = settings[index];
			countdown.jCountdown(setting);
			
			panel.find("input").removeAttr("disabled");
			panel.find(".timeText").val(setting.timeText);
			panel.find(".timeZone").val(setting.timeZone);
			panel.find(".style").removeAttr("checked");
			panel.find(".style[value="+setting.style+"]").attr("checked","checked");
			panel.find(".color").removeAttr("checked");
			panel.find(".color[value="+setting.color+"]").attr("checked","checked");
			panel.find(".widthType").removeAttr("checked");
			setting.width==0 ? panel.find(".widthType[value=auto]").attr("checked","checked") : panel.find(".widthType[value=fixed]").attr("checked","checked");
			setting.width==0 ? panel.find(".width").parents(".group").hide() : panel.find(".width").parents(".group").show();
			panel.find(".width").parents(".group").find(".value").html(countdown.children().width());
			panel.find(".width").slider({min:50, max:900, step:1, value:countdown.children().width()});
			panel.find(".textGroupSpace").slider({min:0, max:100, step:1, value:setting.textGroupSpace});
			panel.find(".textSpace").slider({min:0, max:50, step:1, value:setting.textSpace});
			panel.find(".reflection").attr("checked",setting.reflection);
			panel.find(".reflectionOpacity").slider({min:0, max:100, step:1, value:setting.reflectionOpacity});
			setting.reflection ? panel.find(".reflectionOpacity").parents(".group").show() : panel.find(".reflectionOpacity").parents(".group").hide();
			panel.find(".reflectionBlur").slider({min:0, max:10, step:1, value:setting.reflectionBlur});
			setting.reflection ? panel.find(".reflectionBlur").parents(".group").show() : panel.find(".reflectionBlur").parents(".group").hide();
			panel.find(".dayTextNumber").slider({min:2, max:8, step:1, value:setting.dayTextNumber});
			panel.find(".displayDay").attr("checked",setting.displayDay);
			panel.find(".displayHour").attr("checked",setting.displayHour);
			panel.find(".displayMinute").attr("checked",setting.displayMinute);
			panel.find(".displaySecond").attr("checked",setting.displaySecond);
			panel.find(".displayLabel").attr("checked",setting.displayLabel);
			panel.find(".onFinishRedirectURL").val("");

			panel.find(".textGroupSpace").parent().find(".value").html(setting.textGroupSpace);
			panel.find(".textSpace").parent().find(".value").html(setting.textSpace);
			panel.find(".reflectionOpacity").parent().find(".value").html(setting.reflectionOpacity);
			panel.find(".reflectionBlur").parent().find(".value").html(setting.reflectionBlur);
			panel.find(".dayTextNumber").parent().find(".value").html(setting.dayTextNumber);

			panel.find("input[type=text]").keyup(function(){
				submitPanel(jQuery(this).parents(".page"));
			}).change(function(){
				submitPanel(jQuery(this).parents(".page"));
			});
			panel.find("input[type=checkbox]").change(function(){
				submitPanel(jQuery(this).parents(".page"));
			});
			panel.find("input[type=radio]").click(function(){
				var target = jQuery(this);
				var panel = target.parents(".page");

				panel.find("."+target.attr("class")).removeAttr("checked");
				target.attr("checked","checked");
				
				submitPanel(panel);
			})
			var updateSlider = function(){
				var target = jQuery(this);
				var panel = target.parents(".page");
				var value = target.parent().find(".value");
				value.html(target.slider("value"));
				
				submitPanel(panel);
			}
			panel.find(".slider").slider({
				start:updateSlider, 
				slide:updateSlider, 
				stop:updateSlider
			});
			panel.find(".widthType").click(function(){
				var target = jQuery(this);
				var panel = target.parents(".page");
				var type = panel.find(".widthType[checked]").val();
				
				if(type=="auto"){
					panel.find(".width").parents(".group").hide();
				}else{
					panel.find(".width").parents(".group").show();
				}
			});
			panel.find(".createShortcode").click(function(){
				var target = jQuery(this);
				var page = target.parents(".page");
				
				popupCreateShortcodeWindow(page);
			});
			panel.find(".loadShortcode").click(function(){
				var target = jQuery(this);
				var page = target.parents(".page");
				
				popupLoadShortcodeWindow(page);
			});
			popup.find(".content").click(function(){
				var target = jQuery(this);				
				var popup = target.parents(".popup");
				
				popup.find(".content").select();
			});
			popup.find(".close").click(function(){
				var target = jQuery(this);
				var page = target.parents(".page");
				
				closeAllWindows(page);
			})
			popup.find(".load").click(function(){
				var target = jQuery(this);
				var page = target.parents(".page");
				
				loadShortcodeSetting(page);
				closeAllWindows(page);
			})
		});
	}
	initPanels();
	
	//init backgroundPicker
	jQuery(".backgroundPicker .button").click(function(){
		var target = jQuery(this);
		var page = target.parents(".page");
		var countdown = page.children(".countdown");
		var backgroundPicker = page.children(".backgroundPicker");
		var background = ["background1", "background2", "background3", "background4", "background5"];
		var index = page.find(".backgroundPicker .button").index(target);
		
		for(var i=0; i<background.length; i++){
			countdown.removeClass(background[i]);
		}
		countdown.addClass(background[index]);

		backgroundPicker.children(".button").removeClass("selected");
		target.addClass("selected");
	});
	jQuery(".backgroundPicker .button:first-child").trigger("click");

	//init tip
	jQuery("#tip").appendTo("body");
	jQuery("[tip]").mouseenter(function(){
		var target = jQuery(this);
		var tip = jQuery("#tip");
		var html = target.attr("tip");
		var padding = 5;
		var animateY = 3;
		var animateDuration = 150;
		var width = target.innerWidth()-padding*2;
		var left = (target.offset().left+padding)+"px";
		
		if(target.hasClass("disable")){
			return;
		}

		tip.children(".content").html(html);
		tip.outerWidth(width);

		var top = target.offset().top-tip.outerHeight()-padding;

		tip.stop(true,true).show().css("opacity","0").css("left",left).css("top",top+animateY).animate({
			opacity: 1,
			top: "-="+animateY
		},animateDuration);
	}).mouseleave(function(){
		var target = jQuery(this);
		var tip = jQuery("#tip");
		var html = target.attr("tip");
		var padding = 5;
		var animateY = 3;
		var animateDuration = 150;

		var top = target.offset().top-tip.outerHeight()-padding;

		tip.stop(true,true).css("opacity","1").css("top",top);
		tip.animate({
			opacity: 0,
			top: "+="+animateY
		},animateDuration,function(){
			jQuery(this).hide();
		});
	});
});