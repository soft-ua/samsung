<?php 
/*
Plugin Name: jCountdown Mega Package for WordPress
Plugin URI: http://codecanyon.net/user/ufoufoufo
Description: jCountdown Mega Package for WordPress
Version: 1.0.2
Author: Jason
Author URI: http://codecanyon.net/user/ufoufoufo
License: http://codecanyon.net/licenses/regular_extended
*/

if (!class_exists('jCountdown')){
	class jCountdown{
		//define vars
		var $pluginName;
		var $adminOptionsName;
		var $menuTitle;
		var $menuSlug;
		var $capability;
		
		var $pluginPath;
		var $pluginURL;
		var $pluginAdminPath;
		var $pluginAdminURL;
		var $pluginViewPath;
		var $pluginViewURL;
		var $pluginScriptPath;
		var $pluginScriptURL;
		
		var $adminOptionsDefault;
		
		var $shortcodeInfo;
		
		function jCountdown(){			
			//init vars
			$this->pluginName 			= 'jCountdown';
			$this->adminOptionsName 	= 'jCountdown';
			$this->menuTitle			= 'jCountdown';
			$this->menuSlug 			= 'jcountdown';
			$this->capability 			= 'publish_posts';

			$this->pluginPath 			= plugin_dir_path(__FILE__);
			$this->pluginURL 			= plugin_dir_url(__FILE__);
			$this->pluginAdminPath 		= $this->pluginPath.'admin/';
			$this->pluginAdminURL 		= $this->pluginURL.'admin/';
			$this->pluginViewPath 		= $this->pluginPath.'view/';
			$this->pluginViewURL 		= $this->pluginURL.'view/';
			$this->pluginScriptPath		= $this->pluginPath.'script/';
			$this->pluginScriptURL		= $this->pluginURL.'script/';

			$this->adminOptionsDefault = array();
			$this->adminOptionsDefault['disableAllCountdowns'] = 0;
			$this->adminOptionsDefault['doNotRedirectURL'] = 0;
			
			$this->shortcodeInfo 						= array();
			$this->shortcodeInfo['timetext'] 			= array('name'=>'timeText', 'type'=>'String');
			$this->shortcodeInfo['timezone'] 			= array('name'=>'timeZone', 'type'=>'Number');
			$this->shortcodeInfo['style'] 				= array('name'=>'style', 'type'=>'String');
			$this->shortcodeInfo['color'] 				= array('name'=>'color', 'type'=>'String');
			$this->shortcodeInfo['width'] 				= array('name'=>'width', 'type'=>'Number');
			$this->shortcodeInfo['textgroupspace'] 		= array('name'=>'textGroupSpace', 'type'=>'Number');
			$this->shortcodeInfo['textspace'] 			= array('name'=>'textSpace', 'type'=>'Number');
			$this->shortcodeInfo['reflection'] 			= array('name'=>'reflection', 'type'=>'Boolean');
			$this->shortcodeInfo['reflectionopacity']	= array('name'=>'reflectionOpacity', 'type'=>'Number');
			$this->shortcodeInfo['reflectionblur'] 		= array('name'=>'reflectionBlur', 'type'=>'Number');
			$this->shortcodeInfo['daytextnumber'] 		= array('name'=>'dayTextNumber', 'type'=>'Number');
			$this->shortcodeInfo['displayday'] 			= array('name'=>'displayDay', 'type'=>'Boolean');
			$this->shortcodeInfo['displayhour'] 		= array('name'=>'displayHour', 'type'=>'Boolean');
			$this->shortcodeInfo['displayminute'] 		= array('name'=>'displayMinute', 'type'=>'Boolean');
			$this->shortcodeInfo['displaysecond'] 		= array('name'=>'displaySecond', 'type'=>'Boolean');
			$this->shortcodeInfo['displaylabel'] 		= array('name'=>'displayLabel', 'type'=>'Boolean');
			
			//init
			add_action('init', array(&$this,'init'));
			
			//admin
			add_action('admin_menu', array(&$this,'adminMenu'));
			add_action('admin_enqueue_scripts', array(&$this,'adminEnqueueScripts'));
			
			//view
			add_action('wp_enqueue_scripts', array(&$this,'wpEnqueueScripts'));
		}
		
		//init
		function init(){
			//register style
			wp_register_style('jcountdown', $this->pluginScriptURL.'jcountdown/jcountdown.css');
			wp_register_style('jcountdown-admin-jquery-ui', $this->pluginAdminURL.'jquery/jquery-ui-1.9.1.custom.min.css');
			wp_register_style('jcountdown-admin-shortcode_generator', $this->pluginAdminURL.'shortcode_generator.css');
			wp_register_style('jcountdown-admin-setting', $this->pluginAdminURL.'settings.css');
			
			//register script
			wp_register_script('jcountdown', $this->pluginScriptURL.'jcountdown/jquery.jcountdown.min.js');
			wp_register_script('jcountdown-admin-shortcode_generator', $this->pluginAdminURL.'shortcode_generator.js');
			
			add_action('widget_text', 'do_shortcode');
			add_shortcode('jcountdown', array(&$this,'convertShortcode'));
		}
		
		//admin
		function adminOptions($value=null){
			if($value==null){
				return get_option($this->adminOptionsName, $this->adminOptionsDefault);
			}else{
				update_option($this->adminOptionsName, $value);
			}
		}
		
		function adminEnqueueScripts($hook){
			$menuTitle = strtolower($this->menuTitle);
			
			if($hook=='toplevel_page_'.$this->menuSlug){
				//include style
				wp_enqueue_style('jcountdown');
				wp_enqueue_style('jcountdown-admin-jquery-ui');
				wp_enqueue_style('jcountdown-admin-shortcode_generator');
				
				//include script
				wp_enqueue_script('jcountdown');
				wp_enqueue_script('jquery');
				wp_enqueue_script('jquery-ui-core');
				wp_enqueue_script('jquery-ui-slider');
				wp_enqueue_script('jcountdown-admin-shortcode_generator');
			}else if($hook==$menuTitle.'_page_'.$this->menuSlug .'_settings'){
				//include style
				wp_enqueue_style('jcountdown-admin-setting');
			}
		}
		
		function adminShortcodeGenerator(){
			//include template
			include($this->pluginPath.'admin/shortcode_generator.php');
		}
		
		function adminSettings(){
			//get admin options
			$adminOptions = $this->adminOptions();

			//save change			
			$save = 0;
			
			if($_POST){
				$disableAllCountdowns = array_key_exists('disable_all_countdowns', $_POST) ? $_POST['disable_all_countdowns'] : '';
				$doNotRedirectURL = array_key_exists('do_not_redirect_url', $_POST) ? $_POST['do_not_redirect_url'] : '';
				$resetToDefault = array_key_exists('reset_to_default', $_POST) ? $_POST['reset_to_default'] : '';
				
				$disableAllCountdowns = $disableAllCountdowns=='1' ? 1 : 0;
				$doNotRedirectURL = $doNotRedirectURL=='1' ? 1 : 0;
				$resetToDefault = $resetToDefault=='1' ? 1 : 0;
				
				$adminOptions['disableAllCountdowns'] = $disableAllCountdowns;
				$adminOptions['doNotRedirectURL'] = $doNotRedirectURL;
				
				if($resetToDefault==1){
					$this->adminOptions($this->adminOptionsDefault);
					$adminOptions = $this->adminOptions();
				}else{
					$this->adminOptions($adminOptions);
				}
				
				$save = 1;
			}
			
			//include template
			include($this->pluginPath.'admin/settings.php');
		}
		
		function adminMenu(){
			//add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
			add_menu_page($this->pluginName, $this->menuTitle, $this->capability, $this->menuSlug, array(&$this, 'adminShortcodeGenerator'), $this->pluginAdminURL.'images/icon16.png', null);
			//add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
			add_submenu_page($this->menuSlug, $this->pluginName, 'Shortcode Generator', $this->capability, $this->menuSlug, array(&$this, 'adminShortcodeGenerator'));
			add_submenu_page($this->menuSlug, $this->pluginName, 'Settings', $this->capability, $this->menuSlug.'_settings', array(&$this, 'adminSettings'));
		}
		
		//view
		function convertShortcode($atts, $content=null){
			//get admin options
			$adminOptions = $this->adminOptions();
			
			//check disable all countdowns setting
			if($adminOptions['disableAllCountdowns']==1){
				return '';
			}
			
			$setting = array();
			
			//check do not redirect URL setting
			if($adminOptions['doNotRedirectURL']==0){
				if(array_key_exists('onfinishredirecturl',$atts)){
					if($atts['onfinishredirecturl']!=''){
						$setting []= 'onFinish:function(){location.href="'.urldecode($atts['onfinishredirecturl']).'"}';
					}
				}
			}
			foreach($atts as $key => $value){
				if(array_key_exists($key,$this->shortcodeInfo)){
					if($this->shortcodeInfo[$key]['type']=='String'){
						$value = '"'.$value.'"';
					}
					$setting []= $this->shortcodeInfo[$key]['name'].':'. $value;
				}
 			}
			$settingCode = '{'.join(',',$setting).'}';
			
			$countdownId = 'jCountdown'.rand(1000000000,9999999999);
			$code ='<div id="'.$countdownId.'"></div>';
			$code .='<script>jQuery("#'.$countdownId.'").jCountdown('.$settingCode.')</script>';
			
			return $code;
		}
		
		function wpEnqueueScripts(){
			//include style
    		wp_enqueue_style('jcountdown');
			
			//include script
			wp_enqueue_script('jquery');
    		wp_enqueue_script('jcountdown');
		}
	}
}

if (class_exists('jCountdown')){
	//create jCountdown
	$jCountdown = new jCountdown();
}
?>