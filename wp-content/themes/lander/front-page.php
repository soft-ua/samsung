<?php
/**
 * The custom template for the one-page style front page. Kicks in automatically.
 */

get_header(); ?>
<?php
global $more;
?>
	<div id="primary" class="content-area lander-page">
		<main id="main" class="site-main" role="main">


					<section id="futuretoday">
						<div class="container">
							<div class="row">

								<div class="col-md-6 text-center first padding_none">
									<?php 
									$query = new WP_Query( 'pagename=budushhee-segodnya' );
									// The Loop
									if ( $query->have_posts() ) {
										while ( $query->have_posts() ) {
											$query->the_post();
											the_content();
										}
									}								
									/* Restore original Post Data */
									wp_reset_postdata();
									?>
								</div><!-- col-md-6 first -->



								<div class="col-md-6 padding_right second">
									<div class="row margin_none">
										<div class="col-md-5 padding_none">
											<?php 
												$query = new WP_Query( 'pagename=budushhee-segodnya-tsena' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
											<?php 
												$query = new WP_Query( 'pagename=video' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
										</div><!-- col-md-5 -->
										<?php
				                              $con=mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
					                          // Check connection
					                          if (mysqli_connect_errno())
					                            {
					                            // echo "Failed to connect to MySQLi: " . mysqli_connect_error();
					                            }

					                          // Perform queries 
					                          $result = mysqli_query($con,"SELECT Date FROM wp_countdown ORDER by id DESC LIMIT 1");
					                          $row = $result->fetch_assoc();					                            
					                          
					                          // return $row;

					                          mysqli_close($con);


					                          $date_string = implode("", $row);
					                          $tilltime = substr($date_string,0,-6);
					                          $date_string = $date_string.":00";

												$date = new DateTime($date_string);
												$date = ($date->format('Y/m/d H:i:s'));
					                          
					                          $countdownString0 = str_replace('.', '/', $date_string);
					                          echo "<script language='javascript'>\n";
					                          echo "var tilltime = '".$tilltime."';";
					                          echo "</script>\n";
					                        
					                      ?>
					                      
										<div class="col-md-6 col-md-offset-1 padding_none form_block">
												<div class="row margin_none">
													<div class="countdown">
														<?php echo do_shortcode("[jcountdown timetext='".$date."' timezone='2' style='flip' color='black' width='260' textgroupspace='4' textspace='0' reflection='false' reflectionopacity='10' reflectionblur='0' daytextnumber='2' displayday='true' displayhour='true' displayminute='true' displaysecond='true' displaylabel='true' onfinishredirecturl='']".$date."[/jcountdown]"); ?>
													</div>
												</div>
												<div class="row text-center superprice">
													<p>Заказать по<br> суперцене</p>
												</div>
												<div class="row last">
													<form id="contact_form1" class="form" action="/" novalidate="novalidate" data-selector="form" method="post">
													<div class="form-group padding_left">
														<input id="quantity1" class="form-control" name="quantity1" type="text" placeholder="1шт." /><span class="minus" onclick="minus($('#quantity1'))"></span><span class="plus" onclick="plus($('#quantity1'))"></span>
													</div>
													<div class="form-group padding_left phone">
														<input id="contact_phone1" class="form-control" name="phone1" type="text" placeholder="Телефон" />
													</div>
													<div><button id="contact_submit1" class="btn btn-default btn-block" type="submit">Заказать</button></div>
													</form>
												</div>
											</div><!-- col-md-6 -->
											<?php
												if(isset($_POST['phone1']) && $_POST['phone1'] !== null && isset($_POST['quantity1']) && $_POST['quantity1'] !== null && is_numeric($_POST['phone1']) && is_numeric($_POST['quantity1'])){
											        $phoneNumber1 = $_POST['phone1'];
											        $quantityNumber1 = intval($_POST['quantity1']);
	                        						$today1 = date("Y-m-d H:i:s");  
	                        						
											        $con=mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
													// Check connection
													if (mysqli_connect_errno())
													  {
													  // echo "Failed to connect to MySQLi: " . mysqli_connect_error();
													  }

													// Perform queries 
													mysqli_query($con,"INSERT INTO wp_uji_subscriptions
														(PhoneNumber,QuantityNumber,CreatedDate) 
														VALUES ('$phoneNumber1','$quantityNumber1','$today1')");


													mysqli_close($con);
												}
											?>
										<div class="col-md-12 padding_none">
											<?php 
												$query = new WP_Query( 'pagename=budushhee-segodnya-tekst' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
										</div><!-- col-md-12 -->
									</div><!-- row -->
								</div><!-- col-md-6 first -->

						</div><!-- row -->
						</div><!-- container -->
					</section><!-- #futuretoday -->

					<section id="testimonials">
						<div class="container">
							<div class="row">
								<?php 
								$args = array(
									'posts_per_page' => 3,
									'orderby' => 'rand',
									'category_name' => 'testimonials'
								);
															
								$query = new WP_Query( $args );
								// The Loop
									echo '<div class="col-md-6">';
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										$more = 0;
										echo '<div class="row item_block">';
										echo '<div class="col-md-4 padding_left">';
										echo '<img class="img img-responsive" src='.the_post_thumbnail('testimonial-mug').'>';
										echo '<h3 class="testimonial-name">' . get_the_title() . '</h3>';
										echo '<p class="text-center position">'.get_post_meta($post->ID,'должность',true).'</p>';
										echo '</div>';
										echo '<div class="col-md-8 padding_right">';
										echo '<div class="testimonial-excerpt">';
										the_content('');
										echo '</div>';
										echo '</div>';
										echo '</div>';
									}
								}
									echo '</div>';

								/* Restore original Post Data */
								wp_reset_postdata();
								?>



								<div class="col-md-6 padding_right second">
									<div class="row margin_none">
										<div class="col-md-5 padding_none">
											<?php 
												$query = new WP_Query( 'pagename=budushhee-segodnya-tsena2' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
										</div><!-- col-md-5 -->
											<div class="col-md-6 col-md-offset-1 padding_none form_block">
												<div class="row margin_none">
													<div class="countdown">														
														<?php echo do_shortcode("[jcountdown timetext='".$date."' timezone='2' style='flip' color='black' width='260' textgroupspace='4' textspace='0' reflection='false' reflectionopacity='10' reflectionblur='0' daytextnumber='2' displayday='true' displayhour='true' displayminute='true' displaysecond='true' displaylabel='true' onfinishredirecturl='']".$date."[/jcountdown]"); ?>
													</div>
												</div>
												<div class="row text-center superprice">
													<p>Заказать по<br> суперцене</p>
												</div>
												<div class="row last">
													<form id="contact_form2" class="form" action="/" novalidate="novalidate" data-selector="form" method="post">
													<div class="form-group padding_left">
														<input id="quantity2" class="form-control" name="quantity2" type="text" placeholder="1шт." /><span class="minus" onclick="minus($('#quantity2'))"></span><span class="plus" onclick="plus($('#quantity2'))"></span>
													</div>
													<div class="form-group padding_left phone">
														<input id="contact_phone2" class="form-control" name="phone2" type="text" placeholder="Телефон" />
													</div>
													<div><button id="contact_submit2" class="btn btn-default btn-block" type="submit">Заказать</button></div>
													</form>
												</div>
											</div><!-- col-md-6 -->
											<?php
												if(isset($_POST['phone2']) && $_POST['phone2'] !== null && isset($_POST['quantity2']) && $_POST['quantity2'] !== null && is_numeric($_POST['phone2']) && is_numeric($_POST['quantity2'])){
											        $phoneNumber2 = $_POST['phone2'];
											        $quantityNumber2 = intval($_POST['quantity2']);
	                        						$today2 = date("Y-m-d H:i:s");  

											        $con=mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
													// Check connection
													if (mysqli_connect_errno())
													  {
													  // echo "Failed to connect to MySQLi: " . mysqli_connect_error();
													  }

													// Perform queries 
													mysqli_query($con,"INSERT INTO wp_uji_subscriptions
														(PhoneNumber,QuantityNumber,CreatedDate) 
														VALUES ('$phoneNumber2','$quantityNumber2','$today2')");

													mysqli_close($con);
												}
											?>											
									</div><!-- row -->
								</div><!-- col-md-6 first -->



							</div><!-- row -->
						</div><!-- container -->
					</section><!-- #testimonials -->

					<section id="whywe">
						<div class="container">
							<div class="row">
								<?php 
								$query = new WP_Query( 'pagename=pochemu-imenno-my' );
								$whywe_id = $query->queried_object->ID;

								// The Loop
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										$more = 0;									
										echo '<h2 class="section-title text-center">' . get_the_title() . '</h2>';
										echo '<div class="entry-content col-md-12"><p>';
										the_content('');
										echo '</p></div>';
									}
								}

								/* Restore original Post Data */
								wp_reset_postdata();

								$args = array(
									'post_type' => 'page',
									'orderby' => 'desc',
									'post_parent' => $whywe_id
								);
								$whywe_query = new WP_Query( $args );
								
								echo '<div class="row">';	
								// The Loop
								if ( $whywe_query->have_posts() ) {
																
									while ( $whywe_query->have_posts() ) {
										$whywe_query->the_post();
										$more = 0;
										echo '<div class="col-md-3 text-center">';
											the_post_thumbnail('testimonial-mug');
											the_content('');
										echo '</div>';
									}
								}

								/* Restore original Post Data */
								wp_reset_postdata();
								echo '</div>';
								?>
							</div><!-- row -->
						</div><!-- container -->
					</section><!-- whywe -->

					<section id="thousands">
						<div class="container">
							<div class="row">
							<?php 
							$query = new WP_Query( 'pagename=pochemu-tyisyachi-pokupateley-vyibrali-nas' );
							// The Loop
							if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
									$query->the_post();
									echo '<h2 class="section-title text-center">' . get_the_title() . '</h2>';
									echo '<div class="col-md-3">';
									the_post_thumbnail('');
									echo '</div>';
									echo '<div class="entry-content col-md-4">';
									the_content();
									echo '</div>';
								}
							}

							/* Restore original Post Data */
							wp_reset_postdata();
							?>

								<div class="col-md-5 padding_right second">
									<div class="row margin_none">
										<div class="col-md-5 padding_none">
											<p>Специальная цена<br> Только до <span id="tilltime">01.02.2016</span> всего</p>
											<div class="label_price vert_middle">
												<div>
													<p class="new_price">690 руб.</p>
													<p class="old_price">вместо <span class="throw_lined">1190 руб.</span></p>
												</div>
											</div><!-- label_price -->
										</div><!-- col-md-5 -->
										<div class="col-md-6 col-md-offset-1 padding_none form_block">
											<div class="row margin_none">
												<div class="countdown">		
														<?php echo do_shortcode("[jcountdown timetext='".$date."' timezone='2' style='flip' color='black' width='212' textgroupspace='4' textspace='0' reflection='false' reflectionopacity='10' reflectionblur='0' daytextnumber='2' displayday='true' displayhour='true' displayminute='true' displaysecond='true' displaylabel='true' onfinishredirecturl='']".$date."[/jcountdown]"); ?>
												</div><!-- countdown -->
											</div><!-- row -->
											<div class="row text-center superprice">
												<p>Заказать по<br> суперцене</p>
											</div><!-- row -->
											<div class="row last">
												<form id="contact_form3" class="form" action="/" novalidate="novalidate" data-selector="form" method="post">
												<div class="form-group padding_left">
													<input id="quantity3" class="form-control" name="quantity3" type="text" placeholder="1шт." /><span class="minus" onclick="minus($('#quantity3'))"> </span><span class="plus" onclick="plus($('#quantity3'))"> </span>
												</div>
												<div class="form-group padding_left phone">
													<input id="contact_phone3" class="form-control" name="phone3" type="text" placeholder="Телефон" />
												</div>
												<div><button id="contact_submit3" class="btn btn-default btn-block" type="submit">Заказать</button></div>
												</form>
											</div>
											<?php
												if(isset($_POST['phone3']) && $_POST['phone3'] !== null && isset($_POST['quantity3']) && $_POST['quantity3'] !== null && is_numeric($_POST['phone3']) && is_numeric($_POST['quantity3'])){
											        $phoneNumber3 = $_POST['phone3'];
											        $quantityNumber3 = intval($_POST['quantity3']);
	                        						$today3 = date("Y-m-d H:i:s");

											        $con=mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
													// Check connection
													if (mysqli_connect_errno())
													  {
													  	echo "Failed to connect to MySQLi: " . mysqli_connect_error();
													  }

													// Perform queries 
													mysqli_query($con,"INSERT INTO wp_uji_subscriptions
														(PhoneNumber,QuantityNumber,CreatedDate) 
														VALUES ('$phoneNumber3','$quantityNumber3','$today3')");


													mysqli_close($con);  
												}
											?>
										</div><!-- col-md-6 -->
									</div><!-- row -->
								</div><!-- col-md-6 second -->


						</div><!-- row -->
						</div><!-- container -->
					</section><!-- #thousands -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
