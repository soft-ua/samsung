<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package simone
 */
?>

	</div><!-- #content -->
	<!-- GOTO ARROW BLOCK -->
		<!-- <section id="goto-arrow" class="bg-color2 dark-bg goto-block text-center">
			<div class="container"> 
				<a class="goto top-arrow" href="#"></a>
			</div>
		</section> -->

	<footer class="site-footer" role="contentinfo">
            <?php get_sidebar( 'footer' ); ?>



						<div class="container">
							<div class="row">
								<ul class="nav-justified">
								<?php 
								$query = new WP_Query( 'pagename=nizhnee-menyu' );
								$footerMenu_id = $query->queried_object->ID;

								// The Loop
								// if ( $query->have_posts() ) {
								// 	while ( $query->have_posts() ) {
								// 		$query->the_post();
								// 		$more = 0;									
								// 		echo '<nav class="nav-justified"><ul><li>' . get_the_title() . '</li></ul></nav>';
								// 		echo '<div class="entry-content col-md-12">';
								// 		the_content('');
								// 		echo '</div>';
								// 	}
								// }

								/* Restore original Post Data */
								wp_reset_postdata();

								$args = array(
									'post_type' => 'page',
									'orderby' => 'DESC',
									'post_parent' => $footerMenu_id
								);
								$footerMenu_query = new WP_Query( $args );
								// The Loop
								if ( $footerMenu_query->have_posts() ) {

									while ( $footerMenu_query->have_posts() ) {
										static $link = 0;
										$footerMenu_query->the_post();
										$more = 0;
										echo '<li><button type="button" class="" data-toggle="modal" data-target="#'.$link.'">'.get_the_title() . '</button></li>';
										$link++;
									}
								}
								// The Loop
								if ( $footerMenu_query->have_posts() ) {
									while ( $footerMenu_query->have_posts() ) {
										static $link1 = 0;
										$footerMenu_query->the_post();
										$more = 0;
								?>

									
									<div id="<?php echo($link1)?>" class="modal fade" role="dialog">
									  <div class="modal-dialog">

									    <!-- Modal content-->
									    <div class="modal-content">
									      <div class="modal-header">
									        <button type="button" class="close" data-dismiss="modal">&times;</button>
									      </div>
									      <div class="modal-body">
									        <?php the_content(''); ?>
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
									      </div>
									    </div>

									  </div>
									</div>

								<?php
									$link1++;
									}
								}

								/* Restore original Post Data */
								wp_reset_postdata();
								?>
							</div><!-- row -->
						</div><!-- container -->

	</footer><!-- #colophon -->
	<div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			  <div class="modal-body text-center">
			    <div class="modal-title">
			    </div><!-- modal-title -->
			  </div><!-- modal-body -->
			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->

	<div class="modal fade" id="contact_form4" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			  <div class="modal-body text-center">
			    <div class="modal-title">
			    </div><!-- modal-title -->
					<div class="form_block">
						<div class="row last">
							<form id="contact_form4" class="form" action="/" novalidate="novalidate" data-selector="form" method="post">
							<div class="form-group padding_left">
								<input id="quantity4" class="form-control" name="quantity4" type="text" placeholder="1шт." /><span class="minus" onclick="minus($('#quantity4'))"> </span><span class="plus" onclick="plus($('#quantity4'))"> </span>
							</div>
							<div class="form-group padding_left phone">
								<input id="contact_phone4" class="form-control" name="phone4" type="text" placeholder="Телефон" />
							</div>
							<div><button id="contact_submit4" class="btn btn-default btn-block" type="submit">Заказать</button></div>
							</form>
						</div>
					</div><!-- col-md-6 -->

					<?php
						if(isset($_POST['phone4']) && $_POST['phone4'] !== null && isset($_POST['quantity4']) && $_POST['quantity4'] !== null && is_numeric($_POST['phone4']) && is_numeric($_POST['quantity4'])){
					        $phoneNumber4 = $_POST['phone4'];
					        $quantityNumber4 = intval($_POST['quantity4']);
							$today4 = date("Y-m-d H:i:s");  

					        $con=mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
							// Check connection
							if (mysqli_connect_errno())
							  {
							  // echo "Failed to connect to MySQLi: " . mysqli_connect_error();
							  }

							// Perform queries 
							mysqli_query($con,"INSERT INTO wp_uji_subscriptions
								(PhoneNumber,QuantityNumber,CreatedDate) 
								VALUES ('$phoneNumber4','$quantityNumber4','$today4')");

							mysqli_close($con);
						}
					?>
			  </div><!-- modal-body -->
			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->

</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript" src="/wp-content/themes/lander/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/landerscripts.js"></script>

<script type="text/javascript" src="/wp-content/themes/lander/js/fancybox.pack.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/bootstrap.min.js",></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/smoothscroll.js",></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/jquery.smooth-scroll.min.js",></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/placeholders.jquery.min.js",></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/jquery.counterup.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/waypoints.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/animations/wow.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/jquery.jCounter-0.1.4.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/lander/js/custom.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
	    $(".fancy_text").fancybox({
	        'titlePosition' : 'inside',
	        'transitionIn' : 'none',
	        'transitionOut' : 'none'
	    });


	//Animate CSS + WayPoints javaScript Plugin
	//Example: $(".element").animated("zoomInUp", "zoomOutDown");
	//Author URL: http://webdesign-master.ru
	(function($) {
			$.fn.animated = function(inEffect, outEffect) {
					$(this).css("opacity", "0").addClass("animated").waypoint(function(dir) {
							if (dir === "down") {
									$(this).removeClass(outEffect).addClass(inEffect).css("opacity", "1");
							} else {
									$(this).removeClass(inEffect).addClass(outEffect).css("opacity", "1");
							};
					}, {
							offset: "80%"
					}).waypoint(function(dir) {
							if (dir === "down") {
									$(this).removeClass(inEffect).addClass(outEffect).css("opacity", "1");
							} else {
									$(this).removeClass(outEffect).addClass(inEffect).css("opacity", "1");
							};
					}, {
							offset: -$(window).height()
					});
			};
	})(jQuery);


	    // Анимации
		if(window.innerWidth > 991){

			$('#futuretoday').animated("bounce");		
			$('#testimonials').animated("bounce");		
			$('#whywe').animated("bounce");		
			$('#thousands').animated("bounce");		

		}
	});
	$('.jCountdown.flip .group.day>.label').text('Дней');
	$('.jCountdown.flip .group.hour>.label').text('Часов');
	$('.jCountdown.flip .group.minute>.label').text('Минут');
	$('.jCountdown.flip .group.second>.label').text('Секунд');

	var speed = 250; 

function changeSrc(src){ 
$('#futuretoday .first .wrap_img img').fadeOut(speed); 
setTimeout(function(){$('#futuretoday .first .wrap_img img').attr('src',src).fadeIn(speed)},[speed]); 
return false; 
} 
$('#futuretoday .first .thumbs .col-md-3:first-child img').hover(function(){ 
changeSrc($('#futuretoday .first .thumbs .col-md-3 a img').attr('src')); 
},function(){}); 
$('#futuretoday .first .thumbs .col-md-3:nth-child(2) img').hover(function(){ 
changeSrc($('#futuretoday .first .thumbs .col-md-3:nth-child(2) a img').attr('src')); 
},function(){}); 
$('#futuretoday .first .thumbs .col-md-3:nth-child(3) img').hover(function(){ 
changeSrc($('#futuretoday .first .thumbs .col-md-3:nth-child(3) a img').attr('src')); 
},function(){}); 
$('#futuretoday .first .thumbs .col-md-3:nth-child(4) img').hover(function(){ 
changeSrc($('#futuretoday .first .thumbs .col-md-3:nth-child(4) a img').attr('src')); 
},function(){});

	
</script>


</body>
</html>
