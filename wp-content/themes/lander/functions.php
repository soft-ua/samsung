<?php
/*
	The functions file for the lander child theme
*/

function lander_scripts(){
	if( is_front_page()){
		// wp_enqueue_style('lander-styles', get_stylesheet_directory_uri().'/lander-styles.css');

		wp_enqueue_style('lander-bootstrap', get_stylesheet_directory_uri()."/css/bootstrap.css");
	    wp_enqueue_style('lander-styles', get_stylesheet_directory_uri()."/css/jquery.fancybox.css");
	    // wp_enqueue_style('styles', get_stylesheet_directory_uri()."/css/styles.css");
	    wp_enqueue_style('lander-magnific', get_stylesheet_directory_uri()."/css/magnific-popup.css"); 
	    wp_enqueue_style('lander-iconfont', get_stylesheet_directory_uri()."/css/iconfont-style.css");
	    wp_enqueue_style('lander-animate', get_stylesheet_directory_uri()."/js/animations/animate.css");
	    wp_enqueue_style('lander-styles', get_stylesheet_directory_uri()."/css/bootstrap-datepicker3.min.css");		
	}
}

add_action('wp_enqueue_scripts','lander_scripts');

add_image_size('testimonial-mug', 253, 253, true);

function exclude_testimonials( $query ){
	if( !$query->is_category('testimonials') && $query->is_main_query()){
		$query->set('cat','-5');
	}
}
add_action('pre_get_posts', 'exclude_testimonials');