$(window).load(function() {

    "use strict";

//------------------------------------------------------------------------
//                      PRELOADER SCRIPT
//------------------------------------------------------------------------
    $('#preloader').delay(400).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('#preloader .loading-data').fadeOut(); // will first fade out the loading animation


//------------------------------------------------------------------------
//                      COUNTER SCRIPT
//------------------------------------------------------------------------
   $('.timer').counterUp({
            delay: 20,
            time: 2500
        });


//------------------------------------------------------------------------
//                      COUNTDOWN OPTIONS SCRIPT
//------------------------------------------------------------------------    
    if($('div').is('.countdown')){         
        $('.countdown').jCounter({
            date: "14 february 2016 12:00:00", // Deadline date
            timezone: "Europe/Bucharest",
            format: "dd:hh:mm:ss",
            twoDigits: 'on',
            fallback: function() {console.log("count finished!")}
        });
    }

   
});




$(document).ready(function(){

    $("#tilltime").text(tilltime);
    $("#tilltime2").text(tilltime);
    $("#tilltime3").text(tilltime);
            
    "use strict";

    
    
//------------------------------------------------------------------------
//                      ANCHOR SMOOTHSCROLL SETTINGS
//------------------------------------------------------------------------
    $('a.goto, .navbar .nav a').smoothScroll({speed: 1200});


       
//------------------------------------------------------------------------
//                  SUBSCRIBE 2 FIELDS FORM VALIDATION'S SETTINGS
//------------------------------------------------------------------------          
    $('#contact_form').validate({
        onfocusout: false,
        onkeyup: false,
        rules: {
            quantity: {
                number: true,
                required: true
            },
            phone: {
                number: true,
                required: true
            }
        },
        errorPlacement: function(error, element) {
            element.attr('placeholder', error[0].innerText);
        },
        messages: {
            quantity: {
                required: "Введите количество",
                phone: "Вводите только цифры"
            },
            phone: {
                required: "Введите ваш телефон",
                phone: "Вводите только цифры"
            }
        },
                    
        highlight: function(element) {
            $(element).addClass('error')

        },                    
                    
        success: function(element) {
            // if($(element).class('error'))$(element).removeClass('error');
            
            element
            .text('').addClass('valid')
        }
    }); 
    

        
                
//------------------------------------------------------------------------------------
//                      SUBSCRIBE 2 FIELDS FORM MAILCHIMP INTEGRATIONS SCRIPT
//------------------------------------------------------------------------------------      
    $('#contact_form').submit(function() {
        $('.error').hide();
        $('.error').fadeIn();
        // submit the form
        if($(this).valid()){
            var action = $(this).attr('action');
            $.ajax({
                url: action,
                type: 'POST',
                data: {
                    dataquantity: $('#subscribe_quantity').val(),
                    dataphone: $('#subscribe_phone').val()
                },
                success: function(data) {
                    $('#contact_submit').button('reset');
                    
                    //Use modal popups to display messages
                    $('#modalMessage .modal-title').html('<i class="icon icon-envelope-open"></i>' + data);
                    $('#modalMessage').modal('show');
                    
                },
                error: function() {
                    $('#subscribe_submit').button('reset');
                    
                    //Use modal popups to display messages
                    $('#modalMessage .modal-title').html('<i class="icon icon-ban"></i>Ой!<br>Что-то пошло не так! Попробуйте, пожалуйста, позже.');
                    $('#modalMessage').modal('show');
                    
                }
            });
        }
        return false; 
    });

});


//------------------------------------------------------------------------
//                      NORMALIZE CAROUSEL HEIGHTS FUNCTION
//------------------------------------------------------------------------


$.fn.carouselHeights = function() {

    var items = $(this), //grab all slides
        heights = [], //create empty array to store height values
        tallest; //create variable to make note of the tallest slide
    
    var normalizeHeights = function() {
    
        items.each(function() { //add heights to array
            heights.push($(this).height()); 
        });
        tallest = Math.max.apply(null, heights); //cache largest value
        items.each(function() {
            $(this).css('min-height',tallest + 'px');
        });
    };
    
    normalizeHeights();
    
    $(window).on('resize orientationchange', function () {
        //reset vars
        tallest = 0;
        heights.length = 0;
    
        items.each(function() {
            $(this).css('min-height','0'); //reset min-height
        }); 
        normalizeHeights(); //run it again 
    });




    

};

var digit = 0;
function plus(data){
if($(data).val()==null){
    $(data).val() = 1;
}
digit = $(data).val()*1;
console.log($(data).val());
digit++;          
$(data).val(digit);
}

function minus(data){
if($(data).val()==null){
    $(data).val() = 1;
}
digit = $(data).val()*1;
console.log($(data).val());
digit--;
if(digit<1){
  digit = 1;
}
$(data).val(digit);
}
$('.minus').click(function(){
    minus('#quantity');
});
$('.plus').click(function(){
    plus('#quantity');
});